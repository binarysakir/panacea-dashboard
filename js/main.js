// Preloader
$(window).load(function() {
	$('#status').fadeOut();
	$('#preloader').delay(350).fadeOut('slow');
	$('body').delay(350).css({'overflow':'visible'});
})
// End Preloader

// Show Overlay
$(document).ready(function(){

	$(".add-container").hide();
	$(".add-company").hide();
	$(".switch-add").hide();
	$(".all-info").hide();
	$(".medicine-of-company").hide();

	// SHOW THE SWITCHER

	$(".add").click(function(){
		$(".switch-add").fadeIn();
		$(document).mouseup(function (e){
		    var container = $(".switch-add");
		    if (!container.is(e.target)
		        && container.has(e.target).length === 0){
		        container.fadeOut(500);
		    }
		});
	});

	// SHOW ADD BOTH MEDICINE AND COMPANY

	$(".add-container-click").click(function(){
		$(".add-container").fadeIn();
		$('.switch-add').hide(500);
		$(document).mouseup(function (e){
		    var container = $(".add-container");
		    if (!container.is(e.target)
		        && container.has(e.target).length === 0){
		        container.fadeOut(500);
		    }
		});
	});


	$(".add-company-click").click(function(){
		$(".add-company").fadeIn();
		$('.switch-add').hide(500);
		$(document).mouseup(function (e){
		    var container = $(".add-company");
		    if (!container.is(e.target)
		        && container.has(e.target).length === 0){
		        container.fadeOut(500);
		    }
		});
	});

	$(".all-info-click").click(function(){
		$(".all-info").fadeIn();
		$(document).mouseup(function (e){
		    var container = $(".all-info");
		    if (!container.is(e.target)
		        && container.has(e.target).length === 0){
		        container.fadeOut(500);
		    }
		});
	});

	$(".company-block").click(function(){
		$(".medicine-of-company").fadeIn();
		$(document).mouseup(function (e){
		    var container = $(".medicine-of-company");
		    if (!container.is(e.target)
		        && container.has(e.target).length === 0){
		        container.fadeOut(500);
		    }
		});
	});
	$(".medicine-of-company span").click(function(){
		$(".medicine-of-company").hide();
		$(".all-info").fadeIn();
	});

});


$(document).ready( function(){
    $('#trigger').click( function(event){
        event.stopPropagation();
        $('#drop').fadeToggle();
    });
    $(document).click( function(){
        $('#drop').fadeOut();
    });

});